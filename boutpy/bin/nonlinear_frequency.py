#!/usr/bin/env python3
"""Script to calculate real frequency using the time lag between
adjacent fluctuation peaks in linear simulations

$ linear_frequency.py -h
    for help

OUTPUT: in nonlinear_path,
    - file containing real frequency information in config format
    - figure to show how the frequency is calculated

"""
__author__ = 'Y. Lang'

import argparse
import os
import sys
import re
import numpy as np
import configparser
import matplotlib.pyplot as plt
from matplotlib import rcParams, ticker
from math import floor

from boutpy.boutdata import collect
from boutpy.boutdata.boutgrid import boutgrid
from boutpy.boutdata import Case
from boutpy.boututils.dmp_info import DmpInfo
from boutpy.boututils.compare_inp import parser_config
from boutpy.boututils.functions import get_nth

plt.ion()

parser = argparse.ArgumentParser(
    description="Calculate real frquency in linear simulations.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
    "case_dir", nargs='*', default=['./'],
    help="Path to the BOUT++ case"
)
parser.add_argument(
    "-x", type=float,
    help="Specify the psin to calculate linear growth" +
         "(The x of peak if omitted)"
)
parser.add_argument(
    "-y", nargs="?", type=int, const=-1,
    help="Specify the index of y to calculate linear growth rate" +
         "(The y of peak if omitted)" +
         "(The y of omp if -y with no argument following)"
)
parser.add_argument(
    '-z', type=int,
    help="Specify the index of z to calculate real frequency"
)
parser.add_argument(
    "-v", "--var", default="phi",
    help="The variable to analyse, default is phi"
)
parser.add_argument(
    "-k", "--harmonic", nargs="?", type=int, const=-1,
    help="Remove all except this harmonic" +
         "(Keep all harmonics if ommited)" +
         "(The harmonic with greatest peak zrms if -h with no argument following)"
)
parser.add_argument(
    '-l', '--tl', type=float, default=None,
    help='Starting time in Alfven time, default is t_array[0]'
)
parser.add_argument(
    '-r', '--tr', type=float, default=-1.0,
    help='Ending time in Alfven time, default is -1'
)

args = parser.parse_args()

rcParams.update(
    {"font.size": 11,
     "legend.fontsize": 11,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 1,
     "lines.markersize": 8,
    "savefig.bbox": "tight"}
)

# manage path and parse files
case_path = os.path.realpath(args.case_dir[0])
case = Case(case_path, t_from=1)
di = case.di
case_id = case_path.split('/')[-1]
print("Calculating real frequency of {} ...".format(case_id))
BOUTinp = case.inp
gf = case.gf

# read vars from files
zperiod = int(BOUTinp['ZPERIOD'])
#  in the simulation, keep up to and including this harmonic
low_pass_z = int(BOUTinp['highbeta']['low_pass_z'])
print('low_pass_z = {}'.format(low_pass_z))
timestep = float(BOUTinp["TIMESTEP"])
print("timestep: ", timestep)
psin, yind_imp, yind_omp = gf.get_psin(
    yind='omp', index=True, verbose=False)
t_array = case.t_array
Tbar = di["Tbar"]
print("Simulation end at {} * Tbar".format(t_array[-1]))
print("Tbar = {:.2e} s".format(Tbar))
# The last time step is discarded
t = t_array[0: -1]
t_steps = range(0, len(t))
var_name = args.var

# set Figure and Axes
fig, ax = plt.subplots(2, 2, figsize=[10, 6],
                       facecolor='w', num="growthrate")
ax = ax.flatten()
ax_frqs = ax[3].twinx()
fig.canvas.set_window_title(case_id)
fig.subplots_adjust(hspace=0.23, wspace=0.2, right=0.92)
ax[0].grid(True)
ax[0].set_xlabel(r"$t\ (\tau_A)$")
ax[0].set_ylabel(r"$\lg({}_{{rms}})$".format(var_name))
ax[0].set_title(case.di.get_normalization(var_name))

ax[2].grid(True)
ax[2].set_xlabel(r"$t\ (\tau_A)$")
ax[2].set_ylabel(r"$\gamma\ (\tau_A^{-1})$")

if args.x is not None:
    if args.x < psin[0] or args.x > psin[-1]:
        print("Warning: x is out of the range of psin!")
    x = np.abs(psin - args.x).argmin()

if args.y is not None:
    if args.y < 0:
        y = yind_omp
    else:
        y = args.y

if args.z is not None:
    z = args.z
else:
    z = 0

# [tl, tr] is the time range used to analyse linear growth rate
if args.tl is None:
    tl = t[0]
else:
    tl = args.tl
tl = case.get_tstep(tl, nt_tot=t_steps[-1])
tr = case.get_tstep(args.tr, nt_tot=t_steps[-1])

tr0 = None
# use loop to refine t_range
while True:
    if not tr0 == tr:
        ax[1].clear()
        # re-plot ax[1] and ax[3]
        print("Collecting ", var_name, "[:, :, :, ", tr, "] ...")
        final_xy = case.collect(var_name, tind=tr,
                           nthreads=1).squeeze()
        # If all harmonics are kept
        if args.harmonic is None:
            rms_tr = final_xy.std(axis=-1)
            if args.y is None:
                if args.x is None:
                    x, y = np.unravel_index(
                        np.argmax(rms_tr), rms_tr.shape)
                else:
                    y = np.argmax(rms_tr[x, :])
            else:
                if args.x is None:
                    x = np.argmax(rms_tr[:, y])
            fig.suptitle('n = {} - {}'.format(zperiod, zperiod * low_pass_z))
        # if only one harmonic is kept
        else:
            final_xyn = np.fft.rfft(final_xy, axis=-1)
            # remove n=0 component
            final_xyn[:, :, 0] = 0
            rms_tr = (1 / np.sqrt(2)) * np.abs(final_xyn) / (case.di.nz / 2)
            if args.harmonic < 0:
                if args.y is None:
                    if args.x is None:
                        x, y, harmonic = np.unravel_index(
                            np.argmax(rms_tr), rms_tr.shape)
                    else:
                        y, harmonic = np.unravel_index(
                            np.argmax(rms_tr[x, :, :]), rms_tr[x, :, :].shape)
                else:
                    if args.x is None:
                        x, harmonic = np.unravel_index(
                            np.argmax(rms_tr[:, y, :]), rms_tr[:, y, :].shape)
                    else:
                        harmonic = np.argmax(rms_tr[x, y, :])
            else:
                harmonic = args.harmonic
                if args.y is None:
                    if args.x is None:
                        x, y = np.unravel_index(
                            np.argmax(rms_tr[:, :, harmonic]), rms_tr[:, :, harmonic].shape)
                    else:
                        y = np.argmax(rms_tr[x, :, harmonic])
                else:
                    if args.x is None:
                        x = np.unravel_index(
                            np.argmax(rms_tr[:, y, harmonic]), rms_tr[:, y, harmonic].shape)
            rms_tr = rms_tr[:, :, harmonic].squeeze()
            fig.suptitle('n = {}'.format(zperiod * harmonic))
        print("  Plotting mode structure at tr")
        ax[1].contourf(psin, range(gf["ny"]), rms_tr.transpose(),
                       cmap=plt.get_cmap("jet"), levels=200)
        # ax[1].tick_params(which="both", color='w')
        ax[1].axhline(y=y, ls='--', color='w')
        ax[1].axvline(x=1, color='w', linewidth=1)
        ax[1].set_xlabel(r'$\psi_{n}$')
        ax[1].set_ylabel("yind")
        # ax[1].set_xticklabels([])
        ax[1].plot(psin[x], y, 'wx', ms=15, lw=2, label=
            'x={}, y={}\n'.format(x, y) +
            '$\psi_{{n}}={:.4f}$'.format(psin[x]))
        lgd1 = ax[1].legend(loc='lower right')
        # ax[1].plot(psin[x], y, 'wx', ms=20, lw=2)
        # lgd1 = ax[1].legend(loc="lower right",
        #                     title='x={}, y={}\n'.format(x, y) +
        #                     '$\psi_{{n}}={:.4f}$'.format(psin[x]))
        for text in lgd1.get_texts():
            text.set_color('w')
        ax[1].set_title(
            r"${}_{{rms}}$, t={}$\tau_A$".format(var_name, t[tr]))

        # ax[3].yaxis.set_major_formatter(ticker.FormatStrFormatter("%.1e"))
        # ax[3].grid(True)
        # mode_plot, = ax[3].plot(psin, rms_tr[:, y], 'b-')
        # peak_line = ax[3].axvline(psin[x], lw=1, color='r')
        # ax[3].set_xlabel(r"$\psi_n$")
        # text3 = ax[3].text(0.05, 0.5, r"t={}$\tau_A$".format(t[tr]) +
        #                    "\ny={}\npeak:\nx={}\n".format(y, x) +
        #                    r"$\psi_{{n}}$={:.4f}".format(psin[x]),
        #                    transform=ax[3].transAxes)

    # remove obsolete artists in ax[0] and ax[2]
    try:
        line_rmst.remove()
        line_fit.remove()
        span_rmst.remove()
        lgd0.remove()
        # text0.remove()

        line_gamma.remove()
        line_gamma_mean.remove()
        line_gamma_fit.remove()
        span_gamma.remove()
        lgd2.remove()
        # text2.remove()
    except NameError:
        pass
    print("Collecting " + var_name, "[{}, {}, :, :] ...".format(x, y))
    var_t = case.collect(var_name, xind=x, yind=y,
                    tind=[0, t_steps[-1]]).squeeze()
    print("  Analysing growth rate ...")
    if args.harmonic is None:
        rmst = var_t.std(axis=0).squeeze()
    else:
        var_tn = np.fft.rfft(var_t, axis=0)
        rmst = (1 / np.sqrt(2)) * np.abs(var_tn) / (case.di.nz / 2)
        rmst = rmst[harmonic, :].squeeze()
    rmst[0] = rmst[1]  # To avoid warning about log(0)
    log_rmst = np.log10(rmst)
    # log_rmst = rmst
    # ax[0].set_ylabel(r"${}_{{rms}}$".format(var_name))
    gamma = np.log(10) * np.gradient(log_rmst, timestep)
    gamma_mean = np.mean(gamma[tl: tr + 1])
    gamma_std = np.std(gamma[tl: tr + 1])
    poly = np.polyfit(t[tl: tr + 1], log_rmst[tl: tr + 1], 1, full=True)
    gamma_fit = np.log(10) * poly[0][0]
    linear_fit = t * poly[0][0] + poly[0][1]

    print("  Plotting time evolution of rms(x, y) ...")
    line_rmst, = ax[0].plot(t, log_rmst, 'b-')
    line_fit, = ax[0].plot(t, linear_fit, 'r-', label="fit shaded")
    span_rmst = ax[0].axvspan(t[tl], t[tr], alpha=0.35)
    # lgd0 = ax[0].legend(loc="lower right")
    # text0 = ax[0].text(0.05, 0.8, "$\gamma_{{fit}}$={:.4f}".format(gamma_fit) +
    #                    r"$/\tau_A$" + "\n" + r"$\tau_A$={:.2e}s".format(Tbar),
    #                    transform=ax[0].transAxes)
    lgd0 = ax[0].legend(title=r'$\gamma_{{fit}}={:.4f}$'.format(gamma_fit) +
                              r' $(\tau_{A}^{-1})$' + '\n' + r'$\tau_A={:.2e}$ s'.format(Tbar),
                        loc='lower right')

    line_gamma, = ax[2].plot(t[1:], gamma[1:], 'b-')
    line_gamma_mean = ax[2].axhline(y=gamma_mean, color='g', lw=1, label="mean")
    line_gamma_fit = ax[2].axhline(y=gamma_fit, color='r', lw=1, label="fit")
    span_gamma = ax[2].axvspan(t[tl], t[tr], alpha=0.35)
    # lgd2 = ax[2].legend(loc="upper right")
    # text2 = ax[2].text(0.05, 0.8, "$\gamma_{{mean}}$={:.4f}".format(gamma_mean) +
    #                    r"$/\tau_A$" + "\n" +
    #                    "$std/\gamma$={:.3f}%".format(gamma_std / gamma_mean * 100),
    #                    transform=ax[2].transAxes)
    lgd2 = ax[2].legend(title='$\gamma_{{mean}}={:.4f}$'.format(gamma_mean) +
                              r' $(\tau_{A}^{-1})$' + '\n' +
                              '$std/\gamma={:.3f}$%'.format(gamma_std / gamma_mean * 100),
                        loc='upper right')
    ax[0].relim()
    ax[0].autoscale_view()
    ax[2].relim()
    ax[2].autoscale_view()

    ax[3].clear()
    ax_frqs.clear()
    # ax[3].yaxis.set_major_formatter(ticker.FormatStrFormatter("%.1e"))
    ax[3].grid(True)
    var_z = var_t[z, tl: tr + 1]
    # to avoid log(0)
    if tl == 0:
        var_z[0] = var_z[1]
    t_z = t[tl: tr + 1]
    log_var_z = np.log10(np.abs(var_z))
    ax[3].plot(t_z, log_var_z, 'b-')
    ax[3].tick_params(axis='y', labelcolor='b')
    ax[3].set_xlabel(r'$t\ (\tau_A)$')
    ax[3].set_ylabel(r'$\lg|{}|$'.format(var_name))
    ax[3].relim()
    ax[3].autoscale_view()

    peaks = []
    for it in range(1, tr - tl):
        if np.max([log_var_z[it - 1], log_var_z[it + 1]]) < log_var_z[it]:
            peaks.append(it)
    if len(peaks) > 2:
        log_var_peaks = log_var_z[peaks]
        t_peaks = t_z[peaks]
        ax[3].plot(t_peaks, log_var_peaks, 'k+')
        t_periods = np.gradient(t_peaks, 0.5)
        frqs = 1 / t_periods[1: -1]
        frq_mean = np.mean(frqs)
        frq_std = np.std(frqs)
        ax_frqs.tick_params(axis='y', labelcolor='r')
        ax_frqs.plot(t_peaks[1:-1], frqs, 'r-')
        ax_frqs.set_ylabel(r'$f\ (\tau_{A}^{-1})$')
        ax_frqs.axhline(y=frq_mean, color='r', ls='--',
                        label=r'$f_{{mean}}={:.4f}$'.format(frq_mean))
        lgd3 = ax_frqs.legend(loc='lower right',
                              title='$std/f={:.3f}$%'.format(frq_std / frq_mean * 100))
        ax_frqs.relim()
        ax_frqs.autoscale_view()

    # get the time range for further analysis from command line
    while True:
        want_refine = input("Do you want to further refine the time range " +
                            "for linear growth rate analysis?(y/n)")
        if want_refine in ['y', 'Y', 'yes', 'n', 'N', 'no']:
            break
        print("Wrong option. Choose again!")
    if want_refine in ['n', 'N', 'no']:
        break
    tr0 = tr
    print("Input the new time range:")
    while True:
        try:
            tl = input("\tStarting time in Tbar:")
            tr = input("\tEnding time in Tbar:")
            # to allow indices such as -100,-1
            tl = case.get_tstep(tl, nt_tot=t_steps[-1])
            tr = case.get_tstep(tr, nt_tot=t_steps[-1])
            if not (tl in t_steps and tr in t_steps and tr > tl):
                print("Unrecognized time range. Try again!")
                continue
            break
        except (TypeError, ValueError):
            print("Please input floats. Try again!")
    print("received: tl = {}, tr = {}".format(tl, tr))

# Save the result in config format for further use
print("Saving results to " + case.nonlinear_path)

if args.harmonic is None:
    figname = 'frq{}_{}x{:03d}y{:03d}t{:04d}hall.png'.format(
        case_id, var_name, x,y, int(t[tr]))
elif args.harmonic < 0:
    figname = 'frq{}_{}x{:03d}y{:03d}t{:04d}hmax{:02d}.png'.format(
        case_id, var_name, x, y, int(t[tr]), harmonic)
else:
    figname = 'frq{}_{}x{:03d}y{:03d}t{:04d}h{:02d}.png'.format(
        case_id, var_name, x, y, int(t[tr]), harmonic)
plt.savefig(os.path.join(case.nonlinear_path, figname))


# if args.harmonic is None:
#     plt.savefig(os.path.join(case.nonlinear_path, 'frq{}_{}x{}y{}t{}.png'.format(
#         case_id, var_name, x, y, int(t[tr]))))
# else:
#     plt.savefig(os.path.join(case.nonlinear_path, 'frq{}_{}x{}y{}h{}t{}.png'.format(
#         case_id, var_name, x, y, harmonic, int(t[tr]))))

info = configparser.ConfigParser()
# make the options case-sensitive
info.optionxform = str
info_file = os.path.join(case.nonlinear_path, "nonlinear_frequency")
info.read(info_file)
if case_id not in info.sections():
    info.add_section(case_id)
info.set(case_id, 'x', '{:d}'.format(x))
info.set(case_id, 'y', '{:d}'.format(y))
info.set(case_id, 'timestep', '{:.2f}'.format(timestep))
info.set(case_id, 'time', '{:.2f}'.format(t_array[-1]))
info.set(case_id, 'Tbar', '{:.4e}'.format(Tbar))
info.set(case_id, 'tl', '{:d}'.format(tl))
info.set(case_id, 'tr', '{:d}'.format(tr))
info.set(case_id, 'gamma', '{:.4f}'.format(gamma_fit))
info.set(case_id, 'residual', '{:.4f}'.format(poly[1][0]))
info.set(case_id, 'gamma_mean', '{:.4f}'.format(gamma_mean))
info.set(case_id, 'std_gamma', '{:.4f}'.format(gamma_std))
info.set(case_id, 'std_over_gamma', '{:.4f}'.format(gamma_std / gamma_mean))
info.write(open(info_file, 'w'))

plt.close("all")
