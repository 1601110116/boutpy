#!/usr/bin/env python3
"""Make a movie showing the poloidal cross-section of a given var
"""
__author__ = 'Y.Lang'

import os
import argparse
from math import floor
import matplotlib.pyplot as plt
from matplotlib import animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np

from boutpy.bin.compare_inp import parser_config
from boutpy.boutdata.boutgrid import boutgrid
from boutpy.boututils.dmp_info import DmpInfo
from boutpy.boututils.functions import get_nth
from boutpy.boutdata import Case


def zinterp(var, zind):
    """ Interpolate a 1D periodic function. Polynomial interpolation

    Parameters
    ----------
    var: ndarray. 1D. The next value of var[-1] is assumed to be var[0]
    zind: float. The effective index of the interpolated result

    Returns
    -------
    ret: var[zind]
    """
    var1d: np.ndarray = var.squeeze()
    nz = var1d.size
    z0 = floor(zind)
    p = zind - float(z0)  # p \in [0, 1)
    # Make z0 \in [0, nz-1]
    z0 = ((z0 % nz) + nz) % nz
    zp = (z0 + 1) % nz
    zm = (z0 - 1 + nz) % nz

    ret = 0.5 * p * (p - 1) * var[zm] + \
          (1 + p) * (1 - p) * var[z0] + \
          0.5 * p * (p + 1) * var[zp]

    return ret


def pol_movie(var_name='phi', path='./', tl=0, tr=-1, dt=1, zangle=0.0,
              nlevels=150, shape=True,
              dynamic=True, interval=20, fps=2, metadata=None, bitrate=-1,
              dpi=120, format='gif'):
    """Make an animation of the poloidal cross-section of using contours

    Parameters
    ----------
    var_name: The name of the variable to make animation
    path: the path of the case
    tl: starting time of the movie (in Alfven time)
    tr: ending time of the movie
    dt: the time between adjacent frames (in Alfven time)
    zangle : float, optional, default: 0
        get result at toroidal angle ``zangle`` (in degree).
    nlevels: int, optional, default: 40
        the number of levels to contour/contourf draw.
    shape: bool, optional default: False
        Show the contour in tokamak shape
    dynamic: bool, optional, default: False
        If False, the colorbar does not change among different frames.
        If True, the colorbar is modified according to the minimum and
        maximum values of each frame.
    interval: int, optional, default: 20
        delay between frames in milliseconds.
    fps: number, optional, default: 5
        frames per second in the movie.
    metadata: dict, optional, default: {artist:$USER}
        Dictionary of keys and values for metadata to include in the
        output file. Some keys that may be use include:
        title, artist, genre, subject, copyright, srcform, comment.
    bitrate: number, optional, default: -1
        Specifies the number of bits used per second in the compressed
        movie, in kilobits per second. A higher number means a higher
        quality movie, but at the cost of increased file size.
        -1 means let utility auto-determine.
    dpi: int, optional, default: 100
        Controls the dots per inch for the movie frames.  This combined with
        the figure's size in inches controls the size of the movie.
    format: str, optional, default:'gif'
        The format of the output movie. 'gif' and 'mp4' are available
    x/y: 1D array_like, optional, default: None
        the size should be same as data in x/y direction.
    x/y/zlabel: str, optional, default: None
        labels for x/y/z-axis,
    zleft: bool, optional, default: True
        draw z-axis at left side
    x/ylim: 2-elements data, optional, default: None
        axis limit for x/y-axis
        if None, auto-set to its index: [0, 1, ..., x/ysize-1]
    zlim: 2-elements data, optional, default: None
        axis limit for z-axis
        if None, auto-set to [`vmin`, `vmax`]
    vmin/vmax: number, optional, default: None
        set color limit to [`vmin`, `vmax`]
        if None, auto-set to the range of data
    title: str, optional, default: None
        figure title
        in animation, it is passed to 'title.format(frame_number)'
        or 'index = {}".format(frame_number) if it is None.
    cmap: Colormap, optional, default: plt.cm.bwr
        A cm Colormap instance.
    colorbar: bool, optional, default: True
        if True, plot colorbar
    cticks: 1D array_like, optional, default: None
        colorbar ticks.
        if None, auto-set to evenly spaced interval [`vmin`, `vmax`]
        by factor `ncticks`.
    ncticks: int, optional, default: 5
        the number of ticks of colorbar when `cticks` is not set.
    levels: 1D array_like, optional, default: None
        a list of floating point numbers indicating the level curves
        to draw, in increasing order.
        if None, auto-set to evenly spaced interval [`vmin`, `vmax`]
        by factor `nlevels`.
    output: str, optional, default: None
        filename to store the figure or animation. It should include
        suffix. '.gif' format is recommended.

    **key-word arguments for 2D data**:
    index: int | array_like, default: 1

        - array_like: only using data[:, index] for animation.
        - int: only using data[:, ::index] for animation, the last
            frame is included as well.

    **key-word argments for animation**:

    """
    case_path = os.path.realpath(path)
    case = Case(path)
    data_path = os.path.join(case_path, 'data')
    BOUTinp = parser_config(os.path.join(data_path, 'BOUT.inp'))
    grid_path = os.path.join(case_path, BOUTinp["grid"])
    if not os.path.exists(grid_path):
        raise OSError("Cannot find the grid file specified in BOUT.inp")
    gf = boutgrid(grid_path)
    di = DmpInfo(data_path=data_path)

    t_array = case.t_array
    # the step number of the last step
    t = t_array[:-1]
    t_steps = range(0, len(t))
    timestep = float(BOUTinp['TIMESTEP'])
    tl = case.get_tstep(tl, nt_tot=t_steps[-1])
    tr = case.get_tstep(tr, nt_tot=t_steps[-1])
    dt = floor(float(dt) / timestep)
    if dt < 1:
        dt = 1
    tind = list(range(tl, tr+1, dt))

    Rxy = gf['Rxy']
    Zxy = gf['Zxy']
    psin, yind_imp, yind_omp = gf.get_psin(
        yind='omp', index=True, verbose=False)
    ZPERIOD = float(BOUTinp['ZPERIOD'])
    nx, ny, nz = di.nx, di.ny, di.nz
    var4d = np.zeros([nx, ny, nz, len(tind)])
    for it in range(len(tind)):
        var4d[:, :, :, it] = case.collect(var_name, tind=tind[it],
                                     nthreads=1).squeeze()
    try:
        if "qinty" in gf.keys():
            zShift = gf['qinty']
            print("Using qinty as toroidal shift angle")
        else:
            zShift = gf["zShift"]
            print("Using zShift as toroidal shift angle")
    except KeyError:
        raise ValueError("ERROR: Neither qinty nor zShift found")

    zangle = np.pi * float(zangle) / 180.0
    dz = 2 * np.pi / (ZPERIOD * nz)
    # for a given radial and poloidal position, zangle should
    #  have the z-index of zind
    zind = (zangle - zShift) / dz

    # insert nskip points between adjacent poloidal grid points,
    #  calculated by their zind difference.
    #  nskip is a function of y. Maximum is taken in x direction
    nskip = (np.abs(zShift[:, 1:] - zShift[:, :-1]) / dz - 1).max(axis=0)
    nskip = nskip.round().astype(int)

    # xxx_interp means after interpolation
    ny_interp = np.sum(nskip) + ny
    # the indices of the original grid points
    ypos: np.ndarray = np.append([0], nskip.cumsum()) + np.arange(ny)
    ypos = ypos.astype(int)
    rxy_interp = np.zeros([nx, ny_interp])
    zxy_interp = np.zeros([nx, ny_interp])
    var_interp = np.zeros([nx, ny_interp])

    # values at grid points are already known
    rxy_interp[:, ypos] = Rxy
    zxy_interp[:, ypos] = Zxy
    # for a given radial position and poloidal interval,
    #  dzind is the difference of z-index between adjacent
    #  interpolated points
    dzind = (zind[:, 1:] - zind[:, :-1]) / (nskip + 1.0)

    # for all intervals between adjacent poloidal points
    #  for the y-th interval, the indices of the
    #  interpolated points yind_sub is 1,2,...,nskip
    for y in range(ny - 1):
        yind_sub = np.arange(1, nskip[y] + 1).reshape(1, nskip[y])
        w = yind_sub / (nskip[y] + 1)
        rxy_interp[:, ypos[y] + 1: ypos[y + 1]] = (w * Rxy[:, y + 1][:, None]) + \
                                                  ((1 - w) * Rxy[:, y][:, None])
        zxy_interp[:, ypos[y] + 1: ypos[y + 1]] = (w * Zxy[:, y + 1][:, None]) + \
                                                  ((1 - w) * Zxy[:, y][:, None])

    if shape:
        fig, ax = plt.subplots(figsize=(15, 20))
    else:
        fig, ax = plt.subplots(figsize=(30, 15))
    if dynamic:
        div = make_axes_locatable(ax)
        cax = div.append_axes("right", '5%', '5%')
    vmax = var4d.max()
    vmin = var4d.min()
    if not vmax > vmin:
        vmax += 1
    kwargs = {"levels": np.linspace(vmin, vmax, nlevels, endpoint=True)}
    kwargs['cmap'] = plt.cm.bwr

    fig.subplots_adjust(left=0.2, right=0.8)

    if metadata is None:
        metadata = dict(artist=os.environ['USER'])

    def init():
        ax.cla()
        # ax.set_xlabel('R(m)')
        # ax.set_ylabel('Z(m)')

    def animate(i):
        print('tind = {}'.format(tind[i]))
        ax.cla()
        ax.set_title(r't={:.1f}$\tau_A$'.format(t[tind[i]]))
        if shape:
            ax.axis('equal')
            ax.set_xlabel('R(m)')
            ax.set_ylabel('Z(m)')
        else:
            ax.set_xlabel(r'$\psi_n$')
            ax.set_ylabel(r'iy')
        var3d = var4d[:, :, :, i].squeeze()
        assert np.shape(var3d) == (nx, ny, nz)

        # Calculate var_interp
        # z-interpolate at x-y grid points
        for ix in range(nx):
            for iy in range(ny):
                var_interp[ix, ypos[iy]] = \
                    zinterp(var3d[ix, iy, :], zind[ix, iy])
        # z-interpolate and y-weighted average at interpolated points
        for y in range(ny - 1):
            yind_sub = np.arange(1, nskip[y] + 1).reshape(1, nskip[y])
            # z-index of the interpolated points. dimension is (nx, nskip[y])
            zi = zind[:, y].reshape(nx, 1) + \
                 yind_sub * dzind[:, y][:, None]
            # weight of var3d[y+1]
            w = yind_sub / (nskip[y] + 1)
            for ix in range(nx):
                for iy in range(nskip[y]):
                    var_interp[ix, ypos[y] + yind_sub[0, iy]] = \
                        w[0, iy] * zinterp(var3d[ix, y + 1, :], zi[ix, iy]) + \
                        (1 - w[0, iy]) * zinterp(var3d[ix, y, :], zi[ix, iy])
        if dynamic:
            # update levels for each frame. Fix 0 for white
            vmax = var_interp.max()
            vmin = var_interp.min()
            if not vmax > vmin:
                vmax += 1
            max_abs = np.max([np.abs(vmin), np.abs(vmax)])
            vmax = max_abs
            vmin = -max_abs
            kwargs['levels'] = np.linspace(vmin, vmax, nlevels, endpoint=True)

        if shape:
            ret = ax.contourf(rxy_interp, zxy_interp, var_interp, **kwargs)
            # ax.plot(Rxy[0, :4], Zxy[0, :4], 'k-')
            # ax.plot(Rxy[0, -4:], Zxy[0, -4:], 'k-')
            # ax.plot(Rxy[0, 4:-4], Zxy[0, 4:-4], 'k-')
            # ax.plot(Rxy[-1, :], Zxy[-1, :], 'k-')
            ax.plot(Rxy[0, :], Zxy[0, :], 'k-')
            ax.plot(Rxy[-1, :], Zxy[-1, :], 'k-')
            ixseps1 = gf['ixseps1']
            ax.plot(Rxy[ixseps1, :], Zxy[ixseps1, :], 'k-', linewidth=1)
        else:
            ret = ax.contourf(psin, np.arange(ny_interp), var_interp.T, **kwargs)

        if dynamic:
            cax.cla()
            fig.colorbar(ret, cax=cax)
        return ret

    if not dynamic:
        ret = animate(0)
        cb = fig.colorbar(ret)

    anim = animation.FuncAnimation(
        fig, animate,
        frames=len(tind),
        init_func=init,
        blit=False,
        repeat=True,
        interval=interval,
        repeat_delay=1000,  # milliseconds
    )
    print("Generating the animation ...")

    anim_name = os.path.join(case.pert_path, 'anim_{}_t{:04d}-{:04d}'.format(
        var_name, int(t[tind[0]]), int(t[tind[-1]])))
    if format == 'gif':
        anim.save(anim_name + '.gif',
                 writer='imagemagick', fps=fps, metadata=metadata, bitrate=bitrate,
                 dpi=dpi)
    elif format == 'mp4':
        anim.save(anim_name + '.mp4',
                 writer='imagemagick', fps=fps, metadata=metadata, bitrate=bitrate,
                 dpi=dpi)
    print('The animation has been saved to ' + anim_name)
    # anim.save("{}/{}_t{}-{}.mp4".format(case_path, var_name, tl, tr),
    #           writer='ffmpeg', fps=fps, metadata=metadata, bitrate=bitrate,
    #           dpi=dpi)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='make cross-section animation',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('case_dir', nargs='*', default='./',
                        help='Path to the BOUT++ case')
    parser.add_argument('-v', '--var', default='phi',
                        help='The variable to show, default is phi')
    parser.add_argument('-l', '--tl', type=float, default=0.0,
                        help='Starting time in Alfven time, default is 0')
    parser.add_argument('-r', '--tr', type=float, default=-1.0,
                        help='Ending time in Alfven time, default is -1')
    parser.add_argument('-d', '--dt', type=float, default=1.0,
                        help='Time interval between frames, default is 1')
    parser.add_argument('-z', '--zangle', type=float, default=0.0,
                        help='The z-angle to show. In degree. Default is 0')
    parser.add_argument('-s', '--shape', action='store_true',
                        help='Show the contour in tokamak shape')
    parser.add_argument('-f', '--fps', type=int, default=2,
                        help='Frames per second. Default is 5')
    parser.add_argument('-b', '--bitrate', type=int, default=-1,
                        help='kilobits per second. Auto-determined if not specified')
    parser.add_argument('-D', '--dpi', type=int, default=300,
                        help='Dots per inch for each frame. Default is 100')
    parser.add_argument('-F', '--format', type=str, default='gif',
                        help="The format of the output movie. 'gif' and 'mp4' are available")

    args = parser.parse_args()
    pol_movie(var_name=args.var, path=args.case_dir[0], tl=args.tl, tr=args.tr,
              dt=args.dt, zangle=args.zangle, shape=args.shape,
              fps=args.fps, bitrate=args.bitrate,
              dpi=args.dpi)
