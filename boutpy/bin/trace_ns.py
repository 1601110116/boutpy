#!/usr/bin/env python3

"""Script to plot the time traces of different toroidal mode number
components

$ trace_ns -h
    for help

OUTPUT: in linear_path or nonlinear_path

"""
__author__ = 'Y. Lang'

import os.path
import matplotlib.pyplot as plt
import argparse
import numpy as np
from matplotlib import rcParams

from boutpy.boutdata import Case
from boutpy.visualization import color_list

plt.ion()
parser = argparse.ArgumentParser(
    description='Plot the time traces of different toroidal ' +
    'mode number components',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)
parser.add_argument(
    'case_dir', nargs='*', default=['./'],
    help='Path to the BOUT++ case'
)
parser.add_argument(
    "-x", type=float,
    help="Specify the psin of the point to analyse" +
         "(The x of peak pressure gradient at y if omitted)"
)
parser.add_argument(
    "-y", type=int,
    help="Specify the y-index of the point to analyse" +
         "(The y of omp if omitted)"
)
parser.add_argument(
    "-v", "--var", default="phi",
    help="The variable to analyse, default is phi"
)
parser.add_argument(
    '-l', '--tl', type=float, default=None,
    help='Starting time in Alfven time, default is t_array[0]'
)
parser.add_argument(
    '-r', '--tr', type=float, default=-1.0,
    help='Ending time in Alfven time, default is -1'
)

args = parser.parse_args()

rcParams.update(
    {"font.size": 14,
     "legend.fontsize": 11,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 1,
     "lines.markersize": 8,
    "savefig.bbox": "tight"}
)

case_path = os.path.realpath(args.case_dir[0])
case = Case()
inp = case.inp
gf = case.gf

zperoid = int(inp['ZPERIOD'])
low_pass_z = int(inp['highbeta']['low_pass_z'])
t_array = case.t_array

if args.y is None:
    y = case.gf.yind_omp
else:
    y = args.y

# x is the x-index to analyse
if args.x is None:
    print('collecting P0 to find its peak gradient location')
    P0 = case.collect('P0', yind=y, nthreads=1).squeeze()
    Rx = gf['Rxy'][:, y]
    Bpx = gf['Bpxy'][:, y]
    psix = gf['psixy'][:, y]
    # note that P0 here is normalized
    dP0dpsi = np.gradient(P0, psix)
    grad_psi = Rx * Bpx
    gradP0 = dP0dpsi * grad_psi
    x = gradP0.argmin()
    psin = gf.psin[x, y]
else:
    x, psin = gf.get_xind(args.x)

if args.tl is None:
    tl = t_array[0]
else:
    tl = args.tl
tl = case.get_tstep(tl)
tr = case.get_tstep(args.tr)
var_name = args.var

var_t = case.collect(var_name, xind=x, yind=y, tind=[tl, tr], nthreads=1).squeeze()
var_nt = np.fft.rfft(var_t, axis=0)
rms_nt = (1 / np.sqrt(2)) * np.abs(var_nt) / (case.di.nz / 2)

fig, ax = plt.subplots(figsize=[8,6], facecolor='w')
ax.set_prop_cycle(color=color_list(low_pass_z))
_, norm = case.di.get_normalization(var_name, value=True)
harmonics = np.arange(1, low_pass_z+1)
for n_num in harmonics:
    ax.plot(t_array[tl: tr+1], norm * rms_nt[n_num, :])
ax.legend(zperoid*harmonics, title=r'toroidal mode number', ncol=3)
ax.set_yscale('log')
ax.set_xlabel(r't $\left(\tau_{A}\right)$')
var_latex, var_unit = case.di.get_varname_latex(var_name)

ax.set_ylabel(r'$\left<{}\right>_{{rms}}\ \left({}\right)$'.format(
    var_latex, var_unit))
plt.tight_layout()
outname = os.path.join(case.nonlinear_path,
                       'trace_ns_{}_psin{:.4f}_y{:03d}_t{:04d}-'
                       '{:04d}'.format(var_name, psin, y, int(case.get_time(tl)),
                                       int(case.get_time((tr)))))
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)

