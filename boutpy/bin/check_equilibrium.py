#!/usr/bin/env python3
"""Script to check the equilibriums both in grid file and dmp files
OUTPUT: figures in eq/
"""
__author__ = "Y. Lang"

import os
import matplotlib.pyplot as plt

from boutpy.boutdata import collect
from boutpy.boutdata import boutgrid
from boutpy.boututils import DmpInfo
from boutpy.bin.compare_inp import parser_config
from boutpy.boutdata.field import Field
from matplotlib import ticker

def check_Er(case_path='./'):
    case_path = os.path.realpath(case_path)
    data_path = os.path.join(case_path, 'data')
    ce_path = os.path.join(case_path, 'ce')
    if not os.path.exists(ce_path):
        os.makedirs(ce_path)

    BOUTinp = parser_config(os.path.join(data_path, "BOUT.inp"))
    grid_path = os.path.join(case_path, BOUTinp["grid"])
    if not os.path.exists(grid_path):
        raise OSError("Cannot find the grid file specified in BOUT.inp")
    gf = boutgrid(grid_path)

    Er0_dia_x = collect("Er0_dia_x", nthreads=1)
    di = DmpInfo()
    Va = di["Va"]
    Bbar = di["Bbar"]
    Rxy = gf['Rxy']
    Bpxy = gf['Bpxy']
    Lbar = di['Lbar']
    Bbar = di['Bbar']
    Er0_dia = Er0_dia_x * Va * Bbar * (Rxy/Lbar) * (Bpxy/Bbar)

    E_r = gf["E_r"]
    E_r = E_r.view(Field)
    psin, yind_imp, yind_omp = gf.get_psin(
        yind='omp', index=True, verbose=False)

    fig, ax = plt.subplots(2, 2, figsize=[16, 13],
                           facecolor='w', num='Er')
    ax = ax.flatten()
    fig.suptitle("Er")
    fig.subplots_adjust(hspace=0.5, wspace=0.22, right=0.92)
    ax[0].grid(True)
    ax[2].grid(True)

    # ax[0].plot(psin[2:-2], E_r[2:-2, yind_omp], 'b-', label='grid')
    # ax[0].plot(psin[2:-2], Er0_dia[2:-2, yind_omp], 'r-', label='diamag')
    ax[0].plot(psin[:], E_r[:, yind_omp], 'b-', label='grid')
    ax[0].plot(psin[:], Er0_dia[:, yind_omp], 'r-', label='diamag')
    ax[0].legend(loc='lower left')
    ax[0].set_xlabel(r'$\psi_n$')
    ax[0].set_ylabel(r'$E_{r}$(V/m)')
    ax[0].yaxis.set_major_formatter(ticker.FormatStrFormatter("%.1e"))

    plt.savefig(os.path.join(ce_path, 'check_Er.png'))





