"""return the poloidal cross-section of a given var
"""
__author__ = 'Y.Lang'

import os
from math import floor
import numpy as np

from boutpy.boutdata import Case
from boutpy.boutdata import boutgrid
from boutpy.bin.pol_movie import zinterp


def poloidal_slice(var3d, case_path='./', zangle=0.0):
    """
    Calculate the poloidal cross-section of a given var.
    For the algorithm, please refer to bin/pol_movie.py
    Parameters
    ----------
    var3d: The original variable to cut, whose 3 dimensions are x, y, and z respectively
    case_path: The path of the case of var3d
    zangle: Get the result at toroidal angle ''zangle'' (in degree)

    Returns
    -------
    rxy_interp: The Rxy of the var slice
    zxy_interp: The Zxy of the var slice
    var_interp: The slice of the var

    """

    # read from files
    case = Case(case_path)
    Rxy = case.gf['Rxy']
    Zxy = case.gf['Zxy']
    try:
        if "qinty" in case.gf.keys():
            zShift = case.gf['qinty']
            print("Using qinty as toroidal shift angle")
        else:
            zShift = case.gf["zShift"]
            print("Using zShift as toroidal shift angle")
    except KeyError:
        raise ValueError("ERROR: Neither qinty nor zShift found")
    ZPERIOD = float(case.inp['ZPERIOD'])
    nx, ny, nz = case.di.nx, case.di.ny, case.di.nz

    # calculate the vars to use
    zangle = np.pi * float(zangle) / 180.0
    dz = 2 * np.pi / (ZPERIOD * nz)
    zind = (zangle - zShift) / dz
    nskip = (np.abs(zShift[:, 1:] - zShift[:, :-1]) / dz - 1).max(axis=0)
    nskip = nskip.round().astype(int)

    ny_interp = np.sum(nskip) + ny
    ypos: np.ndarray = np.append([0], nskip.cumsum()) + np.arange(ny)
    ypos = ypos.astype(int)
    rxy_interp = np.zeros([nx, ny_interp])
    zxy_interp = np.zeros([nx, ny_interp])
    var_interp = np.zeros([nx, ny_interp])

    rxy_interp[:, ypos] = Rxy
    zxy_interp[:, ypos] = Zxy
    for ix in range(nx):
        for iy in range(ny):
            var_interp[ix, ypos[iy]] = \
                zinterp(var3d[ix, iy, :], zind[ix, iy])

    dzind = (zind[:, 1:] - zind[:, :-1]) / (nskip + 1.0)


    for y in range(ny - 1):
        yind_sub = np.arange(1, nskip[y] + 1).reshape(1, nskip[y])
        w = yind_sub / (nskip[y] + 1)
        rxy_interp[:, ypos[y] + 1: ypos[y + 1]] = (w * Rxy[:, y + 1][:, None]) + \
                                                  ((1 - w) * Rxy[:, y][:, None])
        zxy_interp[:, ypos[y] + 1: ypos[y + 1]] = (w * Zxy[:, y + 1][:, None]) + \
                                                  ((1 - w) * Zxy[:, y][:, None])
        zi = zind[:, y].reshape(nx, 1) + \
             yind_sub * dzind[:, y][:, None]

        w = yind_sub / (nskip[y] + 1)
        for ix in range(nx):
            for iy in range(nskip[y]):
                var_interp[ix, ypos[y] + yind_sub[0, iy]] = \
                    w[0, iy] * zinterp(var3d[ix, y + 1, :], zi[ix, iy]) + \
                    (1 - w[0, iy]) * zinterp(var3d[ix, y, :], zi[ix, iy])

    return rxy_interp, zxy_interp, var_interp
