"""Case class
    provide easy manage of files and data when the case has
    the structure as follow:
    case/   -- data/    --BOUT.inp
                        --BOUT.dmp.*.nc
            -- figs
            -- config data
    The grid file is assumed to be at the place specified by
        the [grid] option in BOUT.inp
"""

import os
from collections import OrderedDict
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import rcParams, ticker
import numpy as np
import sys
from multiprocessing.dummy import Pool
from multiprocessing import cpu_count
import glob
import time
from math import floor
import matplotlib

from boutpy.boutdata import collect
from boutpy.boutdata import boutgrid
from boutpy.boututils.dmp_info import DmpInfo
from boutpy.bin.compare_inp import parser_config
from boutpy.boutdata.field import Field
from boutpy.boututils import DataFile
from boutpy.boutdata.collect import check_range
from boutpy.boututils.functions import get_nth


rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

class Case(object):
    def __init__(self, case_path='./', t_from=0):
        self.t_from = t_from
        self.case_path = os.path.realpath(case_path)
        self.data_path = os.path.join(self.case_path, 'data')
        if not os.path.isdir(self.data_path):
            raise OSError("Invalid case path")
        # path for equilibrium figs and ConfigObj files
        self.eq_path = os.path.join(self.case_path, 'eq')
        if not os.path.exists(self.eq_path):
            os.mkdir(self.eq_path)
        self.pert_path = os.path.join(self.case_path, 'pert')
        if not os.path.exists(self.pert_path):
            os.mkdir(self.pert_path)
        self.di = DmpInfo(data_path=self.data_path, t_from=t_from)
        self.paths, self.tind_start, self.tind_end = self.find_restart_files()
        self.t_array, self.nt = self.build_t_array()
        self.inp = parser_config(
            os.path.join(self.data_path, "BOUT.inp"))
        if self.inp['highbeta']['nonlinear'] in ['False', 'false']:
            self.linear_path = os.path.join(case_path, 'linear')
            if not os.path.exists(self.linear_path):
                os.mkdir(self.linear_path)
        else:
            self.nonlinear_path = os.path.join(case_path, 'nonlinear')
            if not os.path.exists(self.nonlinear_path):
                os.mkdir(self.nonlinear_path)
        self.grid_path = os.path.join(self.case_path, self.inp["grid"])
        if not os.path.exists(self.grid_path):
            raise OSError(
                "Cannot find the grid file specified in BOUT.inp")
        self.gf = boutgrid(self.grid_path)

    def find_restart_files(self):
        paths = [self.case_path]
        tind_start = [0]
        tind_end = [self.di.nt]
        restart_folders = glob.glob(os.path.join(paths[-1], 'restart_*'))
        while restart_folders:
            restart_folder = restart_folders[0]
            if len(restart_folders) > 1:
                print('Warning: Using {} in {}'.format(restart_folder, paths[-1]))
            try:
                f_tmp = DataFile(os.path.join(paths[-1], restart_folder, 'data',
                                              'BOUT.dmp.{}.nc'.format(self.t_from)))
            except FileNotFoundError:
                break
            t_tmp = f_tmp.read('t_array')
            f_tmp.close()
            len_t = len(t_tmp) - 1
            # if we can only find the initial time step in the dmp file
            if len_t < 1:
                break
            # at least one time step has finished, so the dmp files in this restart
            #   folder provide valid data.
            paths.append(os.path.join(paths[-1], restart_folder))
            tind_start.append(tind_start[-1] + int(restart_folder.split('_')[-1]))
            tind_end[-1] = tind_start[-1] - 1
            tind_end.append(tind_start[-1] + len_t)
            restart_folders = glob.glob(os.path.join(paths[-1], 'restart_*'))
        return paths, tind_start, tind_end

    def build_t_array(self):
        nt = self.tind_end[-1]
        t_array = np.zeros(nt + 1)
        for i in range(len(self.paths)):
            f_tmp = DataFile(os.path.join(self.paths[i], 'data',
                                          'BOUT.dmp.{}.nc'.format(self.t_from)))
            t_tmp = f_tmp.read('t_array')
            f_tmp.close()
            t_array[self.tind_start[i]: self.tind_end[i] + 1] \
                = t_tmp[0: self.tind_end[i] - self.tind_start[i] + 1]
        return t_array, nt

    def get_tstep(self, time, nt_tot=None):
        # the last time step is considered as, t=-1\tau_A
        if nt_tot is None:
            nt_tot = self.nt
        start_step = int(self.t_array[0] / self.di.timestep)
        end_step = start_step + nt_tot
        tstep = int(get_nth(floor(float(time) / self.di.timestep), end_step)) - start_step
        return max(0, tstep)

    def get_time(self, tstep):
        return self.t_array[tstep]

    def check_Er(self, yind=None):
        Er0_dia_x = collect("Er0_dia_x", path=self.data_path, nthreads=1)
        # Er0_dia_x = collect("Er0_x", path=self.data_path, nthreads=1)
        Rxy = self.gf['Rxy']
        Zxy = self.gf['Zxy']
        Bpxy = self.gf['Bpxy']
        Lbar = self.di['Lbar']
        Bbar = self.di['Bbar']
        Va = self.di['Va']
        Er0_dia = Er0_dia_x * Va * Bbar * (Rxy / Lbar) * (Bpxy / Bbar)

        E_r = self.gf["E_r"]
        E_r = E_r.view(Field)
        psin, yind_imp, yind_omp = self.gf.get_psin(
            yind='omp', index=True, verbose=False)

        fig, ax = plt.subplots(2, 2, figsize=[13, 16], facecolor='w', num='Er')
        ax = ax.flatten()
        fig.suptitle("Er")
        fig.subplots_adjust(hspace=0.5, wspace=0.22, right=0.92)
        ax[0].grid(True)
        ax[2].grid(True)

        # ax[0].plot(psin[2:-2], E_r[2:-2, yind_omp], 'b-', label='grid')
        # ax[0].plot(psin[2:-2], Er0_dia[2:-2, yind_omp], 'r-', label='diamag')
        ax[0].plot(psin[:], E_r[:, yind_omp], 'b-', label=r'$E_{r}$ in gf')
        ax[0].plot(psin[:], Er0_dia[:, yind_omp], 'r-', label=r'$E_{r,dia0}$')
        ax[0].legend(loc='lower left')
        ax[0].set_xlabel(r'$\psi_n$')
        ax[0].set_ylabel(r'$E_{r}$(V/m)')
        ax[0].yaxis.set_major_formatter(ticker.FormatStrFormatter("%.1e"))
        ax[0].set_title(r'at omp')

        if not yind is None:
            ax[2].plot(psin[:], E_r[:, yind], 'b-', label=r'$E_{r}$ in gf')
            ax[2].plot(psin[:], Er0_dia[:, yind], 'r-', label=r'$E_{r,dia0}$')
            ax[2].legend(loc='lower left')
            ax[2].set_xlabel(r'$\psi_n$')
            ax[2].set_ylabel(r'$E_{r}$(V/m)')
            ax[2].yaxis.set_major_formatter(ticker.FormatStrFormatter("%.1e"))
            ax[2].set_title(r'at yind={:d}'.format(yind))

        ax[1].contourf(Rxy, Zxy, E_r, cmap=plt.get_cmap('jet'), levels=200)
        ax[1].set_title(r'$E_{r}$ in gf (V/m)')
        ax[1].set_xlabel('R(m)')
        ax[1].set_ylabel('Z(m)')

        ax[3].contourf(Rxy, Zxy, Er0_dia, cmap=plt.get_cmap('jet'), levels=200)
        ax[3].set_title(r'$E_{r,dia0}$ (V/m)')
        ax[3].set_xlabel('R(m)')
        ax[3].set_ylabel('Z(m)')

        plt.savefig(os.path.join(self.eq_path, 'check_Er.png'))

    def check_bxcv(self):
        fig, ax = plt.subplots(2, 2)
        ax = ax.flatten()
        ct0 = ax[0].contourf(self.gf['bxcvx'].T, cmap=cm.get_cmap('jet'), levels=100)
        ax[0].set_title('bxcvx')
        ax[0].set_xlabel('ix')
        ax[0].set_ylabel('iy')
        ax[0].yaxis.set_major_formatter(ticker.FormatStrFormatter("%.1e"))
        fig.colorbar(ct0, ax=ax[0], fraction=0.08, aspect=40)

        ax[1].contourf(self.gf['bxcvy'].T, cmap=cm.get_cmap('jet'), levels=100)
        ax[1].set_title('bxcvy')
        ax[1].set_xlabel('ix')
        ax[1].set_ylabel('iy')
        ax[1].yaxis.set_major_formatter(ticker.FormatStrFormatter("%.1e"))
        fig.colorbar(ct0, ax=ax[1], fraction=0.08, aspect=40)

        ax[2].contourf(self.gf['bxcvz'].T, cmap=cm.get_cmap('jet'), levels=100)
        ax[2].set_title('bxcvz')
        ax[2].set_xlabel('ix')
        ax[2].set_ylabel('iy')
        ax[2].yaxis.set_major_formatter(ticker.FormatStrFormatter("%.1e"))
        fig.colorbar(ct0, ax=ax[2], fraction=0.08, aspect=40)

        try:
            fig.savefig(os.path.join(self.eq_path, 'check_bxcv.png'))
        except:
            plt.show()

    def check_scalar(self, var_name, shape=True):
        if var_name in self.di.var_names:
            var = collect(var_name, path=self.data_path, nthreads=1)
        elif var_name in self.gf:
            var = self.gf[var_name]
        else:
            raise Exception('Could not find the given var_name in grid file or dmp files!')
        fig, ax = plt.subplots()
        if shape:
            ct = ax.contourf(self.gf['Rxy'], self.gf['Zxy'], var, cmap=cm.get_cmap('jet'), levels=150)
        else:
            psin, yind_imp, yind_omp = self.gf.get_psin(
                yind='omp', index=True, verbose=False)
            ct = ax.contourf(psin, np.arange(self.di.ny), var.transpose(),
                             cmap=cm.get_cmap('jet'), levels=150)
        ax.set_title(var_name)
        ax.set_xlabel('R (m)')
        ax.set_ylabel('Z (m)')
        ax.axis('equal')
        fig.colorbar(ct, ax=ax, fraction=0.08, aspect=40)
        try:
            if shape:
                fig.savefig(os.path.join('check_' + var_name + '_shape.png'), dpi=80)
            else:
                fig.savefig(os.path.join('check_' + var_name + '.png'), dpi=80)
        except:
            plt.show()

    def fixxt_yrange(self, var_name, tl, tr, xind, y0=None, y1=None, nthreads=None, dpi=200):
        matplotlib.use('Agg')
        di = self.di
        t0 = self.get_tstep(tl)
        t1 = self.get_tstep(tr)
        if y0 is None:
            y0 = 0
        if y1 is None:
            y1 = di.ny
        gf = self.gf
        dz = 2. * np.pi / float(di.zperiod * di.nz)
        deltaz = dz * np.arange(di.nz).reshape(1, di.nz)
        deltaz = np.tile(deltaz, (y1 - y0, 1))
        yind = np.arange(y0, y1).reshape(y1 - y0, 1)
        yind = np.tile(yind, (1, di.nz))
        folder_name = os.path.join(self.pert_path, var_name + '_ix{:03d}'.format(xind) +
                      '_iy{:02d}-{:02d}'.format(y0, y1))
        if not os.path.exists(folder_name):
            os.mkdir(folder_name)
        for it in range(t1 - t0):
            print(t0 + it)
            var: Field = self.collect(var_name, xind=xind, yind=[y0, y1 - 1], tind=t0 + it,
                                 nthreads=nthreads).squeeze().T
            var.contourf(x=deltaz, y=yind, levels=150,
                         xlabel=r'$\Delta z$', ylabel='iy', kind='contourf')
            fig = plt.gcf()
            fig.savefig(os.path.join(folder_name, var_name +
                                     'yz{:04d}.png'.format(int(self.get_time(t0 + it)))), dpi=dpi)

    def fixyt_xrange(self, var_name, tl, tr, yind, x0=None, x1=None, nthreads=None, dpi=200):
        matplotlib.use('Agg')
        di = self.di
        t0 = self.get_tstep(tl)
        t1 = self.get_tstep(tr)
        if x0 is None:
            x0 = 0
        if x1 is None:
            x1 = di.nx
        gf = self.gf

        psin = gf.get_psin(yind="omp")
        # psin = psin[x0: x1]
        psin = np.reshape(psin, (1, di.nx))
        psin = np.tile(psin, (di.nz, 1))
        psin = psin[:, x0:x1]
        folder_name = os.path.join(self.pert_path, var_name + "_iy{:02d}".format(yind) +
                                   "_ix{:03d}-{:03d}".format(x0, x1))
        if not os.path.exists(folder_name):
            os.mkdir(folder_name)
        try:
            if "qinty" in gf.keys():
                zShift = gf['qinty']
                print("Using qinty as toroidal shift angle")
            else:
                zShift = gf["zShift"]
                print("Using zShift as toroidal shift angle")
        except:
            raise ValueError("ERROR: Neither qinty nor zShift found")
        zShift = zShift[:, yind].reshape((1, di.nx))
        zShift = np.tile(zShift, (di.nz, 1))
        dz = 2. * np.pi / float(di.zperiod * di.nz)
        z = np.linspace(0, dz * di.nz, di.nz)
        z = z.reshape((di.nz, 1))
        z = np.tile(z, (1, di.nx))
        zeta = z + zShift
        zeta = zeta[:, x0:x1]

        print("fixyt_xrange({}, {}, {}, {}) : ".format(var_name, t0, t1, yind))
        for it in range(0, t1 - t0):
            print(t0 + it)
            sys.stdout.flush()
            var_itiy: Field = self.collect(var_name, xind=[x0, x1 - 1], yind=yind,
                                      tind=t0 + it,
                                      nthreads=nthreads).squeeze()
            var_itiy.contourf(var_itiy.T, x=psin, y=zeta, levels=150,
                              xlabel=r"$\psi_{n}$", ylabel=r"$\zeta$",
                              kind="contourf")
            plt.plot(psin[0, :].squeeze(), zeta[0, :].squeeze(), linewidth=0.4)
            plt.plot(psin[-1, :].squeeze(), zeta[-1, :].squeeze(), linewidth=0.4)
            fig = plt.gcf()
            fig.savefig(os.path.join(folder_name, var_name +
                                     "_xzeta{:04d}.png".format(int(self.get_time(t0 + it)))), dpi=dpi)


    def frq_spectrum(self, var_name, tl, tr, psin0, yind0, fmax, z0=0, nthreads=None):
        tl = self.get_tstep(tl)
        tr = self.get_tstep(tr)
        psin, yind_imp, yind_omp = self.gf.get_psin(
            yind='omp', index=True, verbose=False)
        xind0 = np.abs(psin - psin0).argmin()
        var_tr = collect(var_name, path=self.data_path, tind=tr,
                         prefix=self.di.prefix, nthreads=nthreads).squeeze()
        rms_tr = var_tr.std(axis=-1)
        fig, ax = plt.subplots(2, 2, figsize=[16, 9], facecolor='w')
        fig.subplots_adjust(hspace=0.25, wspace=0.25, right=0.92)
        ax = ax.flatten()
        ax[0].contourf(psin, range(self.gf['ny']), rms_tr.transpose(),
                       cmap=plt.get_cmap('jet'), levels=200)
        ax[0].plot(psin[xind0], yind0, 'wx', ms=20,
                   label=r'$\psi_{{n}}={:.4f}$'.format(psin[xind0]) +
                   '\ny={:03d}'.format(yind0))
        lgd = ax[0].legend(loc='lower right')
        for text in lgd.get_texts():
            text.set_color('w')
        ax[0].axvline(x=1.0, lw=1, color='w')
        ax[0].set_title('${}_{{rms}}$ at '.format(var_name) +
                        r'$t={}\tau_{{A}}$'.format(self.di.get_time(tr)))
        ax[0].set_xlabel(r'$\psi_{n}$')
        ax[0].set_ylabel('yind')

        var_t = self.collect(var_name, xind=xind0, yind=yind0,
                        tind=[tl, tr], nthreads=nthreads).squeeze()
        rms_t = var_t.std(axis=0).squeeze()
        ax[1].plot(self.di['t_array'][tl: tr+1], rms_t, 'b-')
        ax[1].grid(True)
        ax[1].set_xlabel(r't ($\tau_{A}$)')
        ax[1].set_ylabel('${}_{{rms}}$'.format(var_name))

        var_z = var_t[z0, :].squeeze()
        ax[3].plot(self.di['t_array'][tl: tr+1], var_z, 'b-')
        ax[3].grid(True)
        ax[3].set_xlabel(r't ($\tau_{A}$)')
        ax[3].set_ylabel(r'$\tilde{{{}}}$'.format(var_name))

        var_f = np.fft.rfft(var_z)
        freq = np.fft.rfftfreq(var_z.shape[-1],
                               float(self.inp['TIMESTEP']) * self.di['Tbar'])
        freq *= 1e-3  # convert from Hz to kHz
        spct = ((1/np.sqrt(2)) * np.abs(var_f) / (tr - tl + 1) * 2) ** 2
        ifmax = np.abs(fmax - freq).argmin()
        freq = freq[:ifmax+1]
        spct = spct[:ifmax+1]
        ax[2].plot(freq, spct, 'b-')
        ax[2].grid(True)
        ax[2].set_xlabel('f (kHz)')
        ax[2].set_ylabel(r'${}^{{2}}$'.format(var_name))


        folder_name = os.path.join(self.pert_path, 'frq_spectrum')
        if not os.path.exists(folder_name):
            os.mkdir(folder_name)
        plt.savefig(os.path.join(folder_name, var_name +
            't{:04d}-{:04d}ix{:03d}iy{:03d}.png'.format(
            int(self.di.get_time(tl)), int(self.di.get_time(tr)),
            xind0, yind0)))

    def collect(self, varname, xind=None, yind=None, zind=None, tind=None,
                yguards=False, info=False,
                nthreads=None, shift=True):
        """Collect a variable from a set of BOUT++ outputs in parallel.

        Parameters
        ----------
        varname : str
            Name of the variable.
        xind, yind, zind, tind: int or list[min, max], optional, default: None
            Range for X/Y/Z/T indices to collect. If it's ``None``, it will
            collect all the data in this dimension.
        path : str, optional, default: "./data"
            Path to data files
        nthreads : int, optional, default: None
            Using ``nthreads`` threads to speed up collocting. If
            ``nthreads=None``, it is set to the number of the cpus in current
            node/system.
        yguards : bool, optional, default: False
            Collect Y boundary guard cells if ``yguards=True``
        shift : bool, optional, default: True
            Shift axis if the variables is **time-dependent**
            .. centered:: [t, x, ...] --> [x, ..., t]
        info : bool, optional, defalt: False
            Print information about collect if ``True``

        Returns
        -------
        collect : Field

        Notes
        -----
        the `shift` option is set to True by default, which means it returns the
        data in [x, y, z, t] order which is different from the previous version
        in [t, x, y, z] order.

        """

        # Search for BOUT++ dump files in NetCDF format
        file_list = glob.glob(os.path.join(self.case_path, 'data', 'BOUT.dmp.nc'))
        if file_list != []:
            print("Single (parallel) data file")
            f = DataFile(file_list[0])  # Open the file
            data = f.read(varname)
            return data.view(Field)

        file_list = glob.glob(os.path.join(self.case_path, 'data', 'BOUT.dmp.*.nc'))
        file_list.sort()
        if file_list == []:
            print("ERROR: No data files found")
            return None

        nfiles = len(file_list)
        # print "Number of files: " + str(nfiles)

        # Read data from the first file
        f = DataFile(file_list[0])

        if not nthreads:
            nthreads = cpu_count()  # get number of cpu
        elif (nthreads < 0) or (nthreads > 2 * cpu_count()):
            raise ValueError("unsuitable 'nthreads' value")

        if info:
            print("nthreads: {}\n".format(nthreads))
        try:
            dimens = f.dimensions(varname)
            ndims = len(dimens)
        except TypeError:
            raise TypeError("ERROR: Variable '" + varname + "' not found")

        if ndims < 2 and varname != "t_array":
            # Just read from file
            # We will handle t_array later
            data = f.read(varname)
            f.close()
            return data.view(Field)

        if ndims > 4:
            raise ValueError("Too many dimensions")

        # These scalars are the *same* between dump files,
        # so just read from one dump file
        mxsub = f.read("MXSUB")
        mysub = f.read("MYSUB")
        mz = f.read("MZ")
        myg = f.read("MYG")

        paths = self.paths
        tind_start = self.tind_start
        tind_end = self.tind_end
        nt = self.nt
        t_array = self.t_array
        if varname == "t_array":
            return t_array.view(Field)

        if info:
            print("totally %d time steps\n" % nt)
            print("mxsub = %d mysub = %d mz = %d\n" % (mxsub, mysub, mz))

        # Get the version of BOUT++ (should be > 0.6 for NetCDF anyway)
        try:
            v = f.read("BOUT_VERSION")

            # 2D decomposition
            nxpe = f.read("NXPE")
            mxg = f.read("MXG")
            nype = f.read("NYPE")
            npe = nxpe * nype

            if info:
                print("BOUT_VERSION: ", v)
                print("nxpe = %d, nype = %d, npe = %d\n" % (nxpe, nype, npe))
                if npe < nfiles:
                    print("WARNING: More files than expected (" + str(npe) + ")")
                elif npe > nfiles:
                    print("WARNING: Some files missing. Expected " + str(npe))

            nx = nxpe * mxsub + 2 * mxg
        except KeyError:
            print("BOUT++ version : Pre-0.2")
            # Assume number of files is correct
            # No decomposition in X
            nx = mxsub
            mxg = 0
            nxpe = 1
            nype = nfiles

        if yguards:
            ny = mysub * nype + 2 * myg
        else:
            ny = mysub * nype

        f.close()

        xind = check_range(xind, 0, nx - 1, "xind")
        yind = check_range(yind, 0, ny - 1, "yind")
        zind = check_range(zind, 0, mz - 2, "zind")
        tind = check_range(tind, 0, nt - 0, "tind")

        xsize = xind[1] - xind[0] + 1
        ysize = yind[1] - yind[0] + 1
        zsize = zind[1] - zind[0] + 1
        tsize = tind[1] - tind[0] + 1

        if info:
            print("xind = {}, yind = {}, zind = {}, tind = {}".format(
                xind, yind, zind, tind))

        # Map between dimension names and output size
        sizes = {'x': xsize, 'y': ysize, 'z': zsize, 't': tsize}

        # Create a list with size of each dimension
        ddims = map(lambda d: sizes[d], dimens)

        # Create the data array
        data = np.zeros(list(ddims))

        # determine index range of x,y-processor
        r_pe_xind = [int(float(xind[0] - mxg) / mxsub),
                     int(float(xind[1] - mxg) / mxsub)]
        if yguards:
            r_pe_yind = [int(float(yind[0] - myg) / mysub),
                         int(float(yind[1] - myg) / mysub)]
        else:
            r_pe_yind = [int(float(yind[0]) / mysub), int(float(yind[1]) / mysub)]

        # check boundary
        r_pe_xind = np.min([[nxpe - 1, nxpe - 1], r_pe_xind], axis=0)
        r_pe_xind = np.max([[0, 0], r_pe_xind], axis=0)
        r_pe_yind = np.min([[nype - 1, nype - 1], r_pe_yind], axis=0)
        r_pe_yind = np.max([[0, 0], r_pe_yind], axis=0)
        # thread size used in cxx source code
        # x_nthreads_src = r_pe_xind[1] - r_pe_xind[0] + 1
        # y_nthreads_src = r_pe_yind[1] - r_pe_yind[0] + 1

        # index range of dump files in which the target data locate
        r_nfile = np.array([
            [iny * nxpe + inx for inx in range(r_pe_xind[0], r_pe_xind[1] + 1)]
            for iny in range(r_pe_yind[0], r_pe_yind[1] + 1)])
        ind_nfile = r_nfile.flatten()
        size_nfile = ind_nfile.size

        if info:
            print("Processor range: x = {}, y = {}".format(r_pe_xind, r_pe_yind))
            print("Index array of dump files:")
            print("   shape = {}, size = {}\n".format(r_nfile.shape, size_nfile))
            if info == 2:
                print("Indices of dump files:\n{}\n".format(r_nfile))

        # processing ``index`` file
        def processor(index):
            # Get X and Y processor indices
            pe_yind = int(index / nxpe)
            pe_xind = index % nxpe

            # Get local ranges
            if yguards:
                ymin = yind[0] - pe_yind * mysub
                ymax = yind[1] - pe_yind * mysub
            else:
                ymin = yind[0] - pe_yind * mysub + myg
                ymax = yind[1] - pe_yind * mysub + myg

            xmin = xind[0] - pe_xind * mxsub
            xmax = xind[1] - pe_xind * mxsub

            inrange = True

            if yguards:
                # Check lower y boundary
                if pe_yind == 0:
                    # Keeping inner boundary
                    if ymax < 0:
                        inrange = False
                    if ymin < 0:
                        ymin = 0
                else:
                    if ymax < myg:
                        inrange = False
                    if ymin < myg:
                        ymin = myg

                # Upper y boundary
                if pe_yind == (nype - 1):
                    # Keeping outer boundary
                    if ymin >= (mysub + 2 * myg):
                        inrange = False
                    if ymax > (mysub + 2 * myg - 1):
                        ymax = (mysub + 2 * myg - 1)
                else:
                    if ymin >= (mysub + myg):
                        inrange = False
                    if ymax >= (mysub + myg):
                        ymax = (mysub + myg - 1)

            else:
                if (ymin >= (mysub + myg)) or (ymax < myg):
                    inrange = False  # Y out of range

                if ymin < myg:
                    ymin = myg
                if ymax >= mysub + myg:
                    ymax = myg + mysub - 1

            # Check lower x boundary
            if pe_xind == 0:
                # Keeping inner boundary
                if xmax < 0:
                    inrange = False
                if xmin < 0:
                    xmin = 0
            else:
                if xmax < mxg:
                    inrange = False
                if xmin < mxg:
                    xmin = mxg

            # Upper x boundary
            if pe_xind == (nxpe - 1):
                # Keeping outer boundary
                if xmin >= (mxsub + 2 * mxg):
                    inrange = False
                if xmax > (mxsub + 2 * mxg - 1):
                    xmax = (mxsub + 2 * mxg - 1)
            else:
                if xmin >= (mxsub + mxg):
                    inrange = False
                if xmax >= (mxsub + mxg):
                    xmax = (mxsub + mxg - 1)

            # Number of local values
            nx_loc = xmax - xmin + 1
            ny_loc = ymax - ymin + 1

            # Calculate global indices
            xgmin = xmin + pe_xind * mxsub
            xgmax = xmax + pe_xind * mxsub

            if yguards:
                ygmin = ymin + pe_yind * mysub
                ygmax = ymax + pe_yind * mysub

            else:
                ygmin = ymin + pe_yind * mysub - myg
                ygmax = ymax + pe_yind * mysub - myg

            if not inrange:
                return None  # Don't need this file

            if ndims == 4:
                for i in range(len(paths)):
                    if tind[0] > tind_end[i]:
                        continue
                    elif tind[1] < tind_start[i]:
                        break
                    # in the current file, we have data for [tmin, tmax]
                    tmin = max(tind_start[i], tind[0])
                    tmax = min(tind[1], tind_end[i])
                    filename = os.path.join(paths[i], 'data', 'BOUT.dmp.' + str(index) + '.nc')
                    f = DataFile(filename)
                    d = f.read(varname, ranges=[tmin - tind_start[i], tmax - tind_start[i] + 1,
                                                xmin, xmax + 1,
                                                ymin, ymax + 1,
                                                zind[0], zind[1] + 1])
                    try:
                        data[tmin - tind[0]: tmax - tind[0] + 1,
                             (xgmin - xind[0]):(xgmin - xind[0] + nx_loc),
                             (ygmin - yind[0]):(ygmin - yind[0] + ny_loc), :] = d
                    except ValueError:
                        # Error due to unmatched shapes,
                        # i.e. the `index file is broken.
                        return index
                    f.close()

            elif ndims == 3:
                # Could be xyz or txy
                if dimens[2] == 'z':  # xyz
                    filename = os.path.join(paths[0], 'data', 'BOUT.dmp.' + str(index) + '.nc')
                    f = DataFile(filename)
                    d = f.read(varname,
                               ranges=[xmin, xmax + 1,
                                       ymin, ymax + 1,
                                       zind[0], zind[1] + 1])
                    data[(xgmin - xind[0]):(xgmin - xind[0] + nx_loc),
                    (ygmin - yind[0]):(ygmin - yind[0] + ny_loc), :] = d
                    f.close()
                else:  # txy
                    for i in range(len(paths)):
                        tmin = max(tind_start[i], tind[0])
                        tmax = min(tind[1], tind_end[i])
                        filename = os.path.join(paths[i], 'data', 'BOUT.dmp.' + str(index) + '.nc')
                        f = DataFile(filename)
                        d = f.read(varname,
                                   ranges=[tmin - tind_start[i], tmax - tind_start[i] + 1,
                                           xmin, xmax + 1,
                                           ymin, ymax + 1])
                        try:
                            data[tmin - tind[0]: tmax - tind[0] + 1,
                                 (xgmin - xind[0]):(xgmin - xind[0] + nx_loc),
                                 (ygmin - yind[0]):(ygmin - yind[0] + ny_loc)] = d
                        except ValueError:
                            # Error due to unmatched shapes.
                            return index
                        f.close()
            elif ndims == 2:
                # xy
                filename = os.path.join(paths[0], 'data', 'BOUT.dmp.' + str(index) + '.nc')
                f = DataFile(filename)
                d = f.read(varname, ranges=[xmin, xmax + 1,
                                            ymin, ymax + 1])
                data[(xgmin - xind[0]):(xgmin - xind[0] + nx_loc),
                (ygmin - yind[0]):(ygmin - yind[0] + ny_loc)] = d
                f.close()
            # no Error, normal exit
            return 0

        if nthreads == 1:
            # collect var in current thread
            for i in range(size_nfile):
                processor(ind_nfile[i])
                percent = float(i + 1) / size_nfile
                filename = os.path.join(
                    "BOUT.dmp." + str(ind_nfile[i]) + ".nc")
                print("\rProcessing : [{:<20}] {:>6.1%} {}".format(
                    ('=' * int(percent * 20) + '>')[0:20], percent, filename), end="")
                sys.stdout.flush()
            print()

        else:
            try:
                # collect var in parallel
                pool = Pool(processes=nthreads)
                obj_pool = pool.imap(processor, ind_nfile)
                pool.close()
                # progress info
                while True:
                    completed = obj_pool._index
                    percent = float(completed) / size_nfile
                    print("\rProcessing: [{:<20}] {:>6.1%}".format(
                        ('=' * int(percent * 20) + '>')[0:20], percent), end="")
                    sys.stdout.flush()
                    if completed == size_nfile:
                        break
                    time.sleep(0.05)
                print()
                brokenfiles = [i for i in obj_pool if i]
                if brokenfiles:
                    raise ValueError('Broken files')

            except ValueError:
                pool.terminate()
                pool.join()
                print("\n" + "#" * 15 + " WARNING " + "#" * 15)
                print("Oops! Missing data in dump files: {}.".format(brokenfiles))
                print("      Using 't_array' carefully!")
                print("Checking t_array & Recollecting ...\n")
                data = collect(varname, xind=xind, yind=yind, zind=zind,
                               tind=[tind[0], tind[1] - 1],
                               yguards=yguards, info=info,
                               nthreads=nthreads, shift=False, checkt=True)
            except KeyboardInterrupt:
                print("Keyboard Interrupt, terminating workers ...\n")
                data = None
                pool.terminate()
            finally:
                pool.join()

        # Shift axis if var is time-dependent
        if (dimens[0] == u't') and shift:
            if info:
                print("\nShift axis: [t, x, ...] --> [x, ..., t]\n")
            data = np.rollaxis(data, 0, len(dimens))

        print("data shape: ", data.shape)

        return data.view(Field)