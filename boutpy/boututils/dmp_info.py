"""DmpInfo class
"""

__all__ = ["DmpInfo"]

__date__ = "04012020"
__author__ = "Y. Lang"
__email__ = "ylang@pku.edu.cn"

import os
import glob
from boutpy.boututils import DataFile
import matplotlib.pyplot as plt
from boutpy.boutdata.boutgrid import boutgrid
from boutpy.boutdata import collect
from boutpy.boutdata.field import Field
from boutpy.boututils.functions import get_nth
from math import floor
import numpy as np


class DmpInfo(dict):
    """Get some information of the dump files before collection.
    Used to collect less data
    """

    def __init__(self, data_path="./data", t_from=0):
        """

        Parameters
        ----------
        path : str, optional, default: "./data"
            Path to data files
        """
        super().__init__()
        self.t_from = t_from
        prefix = "BOUT.dmp"
        self.data_path = os.path.realpath(data_path)
        self.prefix = prefix
        self.file_list = glob.glob(os.path.join(self.data_path, prefix + "*.nc"))
        self.file_list.sort()
        self.handle = DataFile(self.file_list[t_from])  # data/BOUT.dmp.0.nc
        self.zperiod = self.get_zperiod()
        self.var_names = self.get_var_names()
        self.update_vars()
        self.nx, self.ny, self.nz, self.nt = self.get_size()
        if self.nt >= 1:
            self.timestep = self.get_timestep()


    def get_size(self):
        t_array = self["t_array"]

        nt = len(t_array) - 1  # tind = [0, 1, ..., nt]
        nxpe = self.handle.read("NXPE")
        mxsub = self.handle.read("MXSUB")
        mxg = self.handle.read("MXG")
        nx = nxpe * mxsub + 2 * mxg

        nype = self.handle.read("NYPE")
        mysub = self.handle.read("MYSUB")
        ny = nype * mysub

        mz = self.handle.read("MZ")
        nz = mz - 1

        return nx, ny, nz, nt


    def get_normalization(self, var_name, value=False):
        """Get a string explaining the normalization of a given var.

        Parameters
        ----------
        var_name: the name of the variable in dump files
        value: if True, return the value of the normalizing constant

        Returns
        -------
        norm_str: A string expressing the normalization.
        norm: the normalizing constant (if value=True)
        """
        norm_str = var_name + ' is normalized to '
        if var_name == 'Te':
            norm = self['Tebar']
            norm_str = 'Te is normalized to ' + r'$\bar{{T}}_e={:.1f}\ eV$'.format(norm)
        elif var_name == 'Ti':
            norm = self['Tibar']
            norm_str = 'Ti is normalized to ' + r'$\bar{{T}}_i={:.1f}\ eV$'.format(norm)
        elif var_name == 'Ni':
            norm = self['Nbar'] * self['density'] / 1e20
            norm_str = 'Ni is normalized to ' \
                       + r'$\bar{{n}}={:.1f}\times 10^{{20}}\ m^{{-3}}$'.format(norm)
        elif var_name == 'Psi':
            norm = self['Lbar']
            norm_str = 'Psi is normalized to ' + r'$\bar{{L}}={:.3f}\ m$'.format(norm)
        elif var_name == 'phi':
            norm = self['Va'] * self['Bbar']
            norm_str = 'phi is normalized to ' + r'$V_A*\bar{{B}}={:.3e}\ V$'.format(norm)
        else:
            print('Warning: Unable to get the normalization!')
            norm = np.nan
            norm_str = ''
        if value:
            return norm_str, norm
        else:
            return norm_str


    def get_varname_latex(self, var_name):
        """

        Parameters
        ----------
        var_name: the name of the variable in dump files

        Returns
        -------
        name: the latex code of var_name, used for ylable, titile for legend in latex code
        unit: the unit of the variable multiplied by its normalization
        """

        if var_name == 'Te':
            name = 'T_{{e}}'
            unit = 'eV'
        elif var_name == 'Ti':
            name = 'T_{{i}}'
            unit = 'eV'
        elif var_name == 'Ni':
            name = 'n_{{i}}'
            unit = '10^{{20}}m^{{-3}}'
        elif var_name == 'Psi':
            name = r'\psi'
            unit = 'm'
        elif var_name == 'phi':
            name = r'\phi'
            unit = 'V'
        else:
            raise NotImplementedError('var_name {} is not implemented'.format(var_name))
        return name, unit


    def get_timestep(self):
        t_array = self["t_array"]
        timestep = t_array[-1] - t_array[-2]
        return timestep

    def get_zperiod(self):
        zperiod = self.handle.read("zperiod")
        return zperiod


    def get_var_names(self):
        var_names = self.handle.list()
        return var_names

    def update_vars(self):
        """Update vars that are the same to all dmp files"""
        for var in self.var_names:
            dimensions = self.handle.handle.variables[var].dimensions
            if 'x' in dimensions or 'y' in dimensions:
                continue
            try:
                self[var] = self.handle.read(var)
            except:
                pass


    def get_tstep(self, time, nt_tot=None):
        """Get the number of time step
        Parameters
        ----------
        time: destination time, in Alfven time
        nt_tot: int, default: self.nt. Total number of time steps used.

        Returns
        -------
        tstep: The number of the time step that is just before the input time"""

        if nt_tot is None:
            nt_tot = self.nt
        start_step = int(self['t_array'][0] / self.timestep)
        end_step = start_step + nt_tot
        tstep = int(get_nth(floor(float(time) / self.timestep), end_step)) - start_step
        return max(0, tstep)

    def get_time(self, tstep):
        """Get the time of a given time step
        Parameters
        ----------
        tstep: destination time step

        Returns
        -------
        time: The time at tstep, in Alfven time"""

        t_array = self["t_array"]
        time = t_array[tstep]
        return time


    def plot_profiles(selfs, grid_name, *args, **kwargs):
        gf = boutgrid(grid_name)
        psin, yind_imp, yind_omp = gf.get_psin(index=True)

        Nbar = collect("Nbar")
        density = collect("density")
        N0 = collect("N0")
        Ne0 = collect("Ne0")
        Tibar = collect("Tibar")
        Tebar = collect("Tebar")
        Te0 = collect("Te0")
        Ti0 = collect("Ti0")

        Ni_exp: Field = N0[:, yind_omp] * density * Nbar  # m^-3
        Ne_exp: Field = Ne0[:, yind_omp] * density * Nbar  # eV
        Ti_exp: Field = Ti0[:, yind_omp] * Tibar
        Te_exp: Field = Te0[:, yind_omp] * Tebar

        fig, ax = plt.subplots(2, 2, sharex=True)
        ax = ax.flatten()
        plt.sca(ax[0])
        Ni_exp.plot(newfig=False, x=psin, xlabel=r"$\psi_{n}$", ylabel=r"$n_{i,0}(cm^{-3})$")
        plt.sca(ax[1])
        Ne_exp.plot(newfig=False, x=psin, xlabel=r"$\psi_{n}$", ylabel=r"$n_{e,0}(cm^{-3})$")
        plt.sca(ax[2])
        Ti_exp.plot(newfig=False, x=psin, xlabel=r"$\psi_{n}$", ylabel=r"$T_{i,0}(eV)$")
        plt.sca(ax[3])
        Te_exp.plot(newfig=False, x=psin, xlabel=r"$\psi_{n}$", ylabel=r"$T_{e,0}(eV)$")
        plt.savefig("dmp_nT_profiles.png")
        plt.show()

        Rxy = gf["Rxy"]
        Bpxy = gf["Bpxy"]
        Va = collect("Va")
        Bbar = collect("Bbar")
        Er0_x = collect("Er0_x")
        Er0_exp: Field = Er0_x * Rxy * Bpxy * Va * Bbar
        Er0_exp = Er0_exp[:, yind_omp]
        fig, ax = plt.subplots(2, 2, sharex=True)
        ax = ax.flatten()
        plt.sca(ax[0])
        Er0_exp.plot(newfig=False, x=psin, xlabel=r"$\psi_{n}$", ylabel=r"$E_{r,0}(V/m)$")
        plt.savefig("dmp_EJ_profiles.png")
        plt.show()

