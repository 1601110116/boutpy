import numpy as np
from boutpy.boututils.dmp_info import DmpInfo
import os
import sys
from boutpy.boutdata import Field
from boutpy.boutdata import collect
from boutpy.boutdata import boutgrid
import matplotlib.pyplot as plt


def fixt_zrms(grid, var_name, t0, t1, nthreads=None, dpi=200):
    di = DmpInfo()
    # nt = di.nt
    # if t0 > nt or t1 > nt:
    #     raise ValueError("Maximum time step exceeded.")
    if not (isinstance(t0, int) and isinstance(t1, int)):
        raise TypeError("t0 and t1 should be output time step numbers.")
    if isinstance(grid, str):
        grid = boutgrid(grid)
    psin = None
    if isinstance(grid, boutgrid):
        psin = grid.get_psin(yind="omp")
    folder_name = var_name + "rms"
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)

    print("fixt_zrms({}, {}, {}) : ".format(var_name, t0, t1))
    for it in range(0, t1 - t0):
        print(t0 + it)
        sys.stdout.flush()
        var_it: Field = collect(var_name, tind=t0 + it, path=di.path,
                                prefix=di.prefix, nthreads=nthreads).squeeze()
        var_rms = var_it.rms()
        ct, cbar = var_rms.contourf(var_rms, x=psin, levels=150, xlabel=r"$\psi_{n}$",
                                    ylabel="iy", kind="contourf")
        fig = plt.gcf()
        fig.savefig(os.path.join(folder_name, var_name + "_strct{:04d}.png".format(t0 + it)), dpi=dpi)
        plt.close()


def fixyt_xrange(grid, var_name, t0, t1, yind, x0=None, x1=None, nthreads=None, dpi=200):
    di = DmpInfo()
    if not (isinstance(t0, int) and isinstance(t1, int)):
        raise TypeError("t0 and t1 should be output time step numbers.")
    if x0 is None:
        x0 = 0
    if x1 is None:
        x1 = di.nx
    if isinstance(grid, str):
        gf = boutgrid(grid)
    elif isinstance(grid, boutgrid):
        gf = grid
    else:
        raise TypeError("Wrong grid file")

    psin = gf.get_psin(yind="omp")
    # psin = psin[x0: x1]
    psin = np.reshape(psin, (1, di.nx))
    psin = np.tile(psin, (di.nz, 1))
    psin = psin[:, x0:x1]
    folder_name = var_name + "_iy{:02d}".format(yind) + "_ix{:03d}-{:03d}".format(x0, x1)
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
    try:
        if "qinty" in gf.keys():
            zShift = gf['qinty']
            print("Using qinty as toroidal shift angle")
        else:
            zShift = gf["zShift"]
            print("Using zShift as toroidal shift angle")
    except:
        raise ValueError("ERROR: Neither qinty nor zShift found")
    zShift = zShift[:, yind].reshape((1, di.nx))
    zShift = np.tile(zShift, (di.nz, 1))
    dz = 2. * np.pi / float(di.zperiod * di.nz)
    z = np.linspace(0, dz * di.nz, di.nz)
    z = z.reshape((di.nz, 1))
    z = np.tile(z, (1, di.nx))
    zeta = z + zShift
    zeta = zeta[:, x0:x1]

    print("fixyt_xrange({}, {}, {}, {}) : ".format(var_name, t0, t1, yind))
    for it in range(0, t1 - t0):
        print(t0 + it)
        sys.stdout.flush()
        var_itiy: Field = collect(var_name, xind=[x0, x1 - 1], yind=yind,
                                  tind=t0 + it, path=di.data_path, prefix=di.prefix,
                                  nthreads=nthreads).squeeze()
        var_itiy.contourf(var_itiy.T, x=psin, y=zeta, levels=150,
                          xlabel=r"$\psi_{n}$", ylabel=r"$\zeta$",
                          kind="contourf")
        plt.plot(psin[0, :].squeeze(), zeta[0, :].squeeze(), linewidth=0.4)
        plt.plot(psin[-1, :].squeeze(), zeta[-1, :].squeeze(), linewidth=0.4)
        fig = plt.gcf()
        fig.savefig(os.path.join(folder_name, var_name + "_xzeta{:04d}.png".format(t0 + it)),
                    dpi=dpi)


def fixxt_yrange(grid, var_name, t0, t1, xind, y0=None, y1=None, nthreads=None,
                 dpi=200):
    di = DmpInfo()
    if not (isinstance(t0, int) and isinstance(t1, int)):
        raise TypeError("t0 and t1 should be output time step numbers.")
    if y0 is None:
        y0 = 0
    if y1 is None:
        y1 = di.ny
    if isinstance(grid, str):
        gf = boutgrid(grid)
    elif isinstance(grid, boutgrid):
        gf = grid
    else:
        raise TypeError("Wrong grid file")

    dz = 2. * np.pi / float(di.zperiod * di.nz)
    deltaz = dz * np.arange(di.nz).reshape(1, di.nz)
    deltaz = np.tile(deltaz, (y1-y0, 1))
    yind = np.arange(y0, y1).reshape(y1-y0, 1)
    yind = np.tile(yind, (1, di.nz))
    folder_name = var_name + '_ix{:03d}'.format(xind) + \
                  '_iy{:02d}-{:02d}'.format(y0, y1)
    if not os.path.exists(folder_name):
        os.mkdir(folder_name)
    for it in range(t1 - t0):
        print(t0 + it)
        var: Field = collect(var_name, xind=xind, yind=[y0, y1-1], tind=t0 + it,
                             path=di.data_path, prefix=di.prefix, nthreads=nthreads).squeeze().T
        var.contourf(x=deltaz, y=yind, levels=150,
                     xlabel=r'$\Delta z$', ylabel='iy', kind='contourf')
        fig = plt.gcf()
        fig.savefig(os.path.join(folder_name, var_name + \
                    'yz{:04d}.png'.format(t0+it)), dpi=dpi)
